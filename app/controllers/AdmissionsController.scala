package controllers

import play.api.mvc._
import play.api.libs.json._
import _root_.util.ContextAwareExecutor.Implicits._
import play.api.Logger
import models.Admission
import scala.concurrent.Future
import java.util.concurrent.TimeoutException

class AdmissionsController() extends BaseController {

  def extractAdmission(json: String): List[Admission] = Json.parse(json).as[List[Admission]]

  def byPatientId(id: Long) = Action { implicit request =>
    val r = """[{"id":545,"estado":"ALTA_EFECTIVA","admision":338,"origen":"INT","origenDescripcion":"Internacion","nroInternacion":338,"cobertura":{"financiador":"O.S.D.E.","plan":"PLAN 210","numero":"61089471302","privado":false},"ingreso":"05-01-2016 11:58:00","medico":"Alescio, Jorge Alberto","condicionIva":"GRAVADO","fechaAlta":"06-01-2016 14:32:27","paciente":{"apellido":"SANCHEZ","nombre":"CARLOS","fechaNacimiento":"27-03-1988","documento":"4311861","sexoDto":"MASCULINO","tipoDocumento":"DNI","id":7431},"internacionId":545},{"id":943,"estado":"EN_LA_CAMA","admision":418,"origen":"CA","origenDescripcion":"Ambulatoria","nroInternacion":418,"cobertura":{"financiador":"O.S.D.E.","plan":"PLAN 210","numero":"61089471302","privado":false},"ingreso":"14-01-2016 18:53:00","diagnosticoIngreso":"INFECCION DEL TRACTO URINARIO (ITU)","condicionIva":"EXENTO","paciente":{"apellido":"SANCHEZ","nombre":"RODOLFO HORACIO","fechaNacimiento":"27-03-1940","documento":"4311861","sexoDto":"MASCULINO","tipoDocumento":"DNI","id":7431},"habitacion":"2","cama":"2","internacionId":943}]"""
    Ok(Json.toJson(extractAdmission(r)))
  }

}