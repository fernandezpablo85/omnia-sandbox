package controllers

import play.api.mvc._
import play.api.libs.json._
import play.api.Logger
import _root_.util.ContextAwareExecutor.Implicits._
import scala.concurrent.Future
import _root_.util.JsonUtil
import models.Attachment

abstract class BaseController extends Controller with UserInfoHelper {}

trait UserInfoHelper {
 import models.{ActiveUserInfo,CookieUserInfo,Center,Clinic,SubSpecialty,Specialty,Role,License}

 def info(implicit request: RequestHeader): Option[ActiveUserInfo] = {
    Some(ActiveUserInfo(CookieUserInfo(10,"Juan Carlos","Petruza",List(Center(15,"Sanatorio Trinidad Palermo","TRIPA",Clinic(3,"SA","Sanatorio de la Trinidad Palermo")), Center(21,"Sanatorio Trinidad Ramos Mejia","TRM",Clinic(23,"TRM","Trinidad Ramos Mejia"))),List(SubSpecialty(24,"CLINICA MEDICA","CLINICA MEDICA",Some(Specialty(176,"ALE","ALERGIA","ALERGIA"))), SubSpecialty(77,"TERAPIA INTENSIVA","TERAPIA INTENSIVA",Some(Specialty(176,"ALE","ALERGIA","ALERGIA")))),List(Role(1,"Escribir","Escribir"), Role(2,"Imprimir","Imprimir"), Role(3,"Acceso Externo","Acceso Externo"), Role(4,"Desactivar Autologoff","Desactivar Autologoff")),Some(License(1,"MN","120587"))),Center(21,"Sanatorio Trinidad Ramos Mejia","TRM",Clinic(23,"TRM","Trinidad Ramos Mejia")),SubSpecialty(24,"CLINICA MEDICA","CLINICA MEDICA",Some(Specialty(176,"ALE","ALERGIA","ALERGIA")))))
  }

  def udn(implicit request:RequestHeader): Option[String] =
    info(request).map(_.activeCenter.udnDto.id.toString)

  def specialty(implicit request: RequestHeader): Option[String] =
    info(request).map(_.activeSpecialty.descripcion)
}