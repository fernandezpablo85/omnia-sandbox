package services

import models.{Evolution, Problem, Episode}
import scala.slick.driver.H2Driver.simple._
import scala.slick.jdbc.{StaticQuery => Q}
import scala.concurrent.Future
import com.google.inject.Inject

import play.api.cache.Cache
import play.api.Play.current

class EvolutionService  {

  private val empty = Map.empty[Evolution, List[Long]].withDefaultValue(Nil)

  private def addEvo(evo: Evolution, pl: List[Long]) = {
    val evos = Cache.getAs[Map[Evolution, List[Long]]]("evos").getOrElse(empty)
    Cache.set("evos", evos + (evo -> pl))
  }

  private def evos = Cache.getAs[Map[Evolution, List[Long]]]("evos").getOrElse(empty)

  def save(evolution: Evolution, problemIds: List[Long]) =  {
    val next = evos.size + 1
    addEvo(evolution.copy(id = Some(next)), problemIds)
    Future.successful(next)
  }

  def getByPatientIdAndEpisode(id: Long, episode: Episode): Future[Map[Evolution, List[Long]]] = {
    Future.successful(evos)
  }

}