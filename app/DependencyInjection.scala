import play.api._

import com.google.inject.Guice
import com.google.inject.Singleton
import com.google.inject.AbstractModule
import com.google.inject.Stage
import com.tzavellas.sse.guice.ScalaModule

trait DependencyInjection { self: GlobalSettings =>

  lazy val injector = Guice.createInjector(Stage.PRODUCTION, new ControllerModule())

  override def getControllerInstance[A](clazz: Class[A]) = {
    injector.getInstance(clazz)
  }
}

private class ControllerModule extends ScalaModule {
  import controllers._

  override def configure = {
    install(new ServiceModule)
    bind[AdmissionsController].in[Singleton]
    bind[EvolutionsController].in[Singleton]
    bind[ProblemsController].in[Singleton]
    bind[TimelineController].in[Singleton]
  }
}

private class ServiceModule extends ScalaModule {
  import services._

  override def configure = {
    bind[EvolutionService].in[Singleton]
    bind[ProblemService].in[Singleton]
    bind[TimelineLog].in[Singleton]
  }
}
