package models

/*
  clean formatting 'quirks' we get from external APIs.
*/
object Sanitizers {

  def capitalizeAll(s: String): String =
    s.split("\\s+").map(trimAndCapitalize).mkString(" ")

  def trimAndCapitalize(s: String): String = {
    val trimmed = s.trim

    if (trimmed.length < 2) {
      trimmed
    } else {
      trimmed.head.toUpper + trimmed.tail.toLowerCase
    }
  }

  implicit class SanitizedAdmission(adm: Admission) extends Sanitizer[Admission] {

    def sanitize: Admission = {
      val _personalInfo = sanitize(adm.paciente)
      adm.copy(paciente = _personalInfo)
    }

    private def sanitize(patient: Patient): Patient = {
      val lastName = capitalizeAll(patient.apellido)
      val motherLastName = patient.apellidoCasada.map(capitalizeAll)
      val name = capitalizeAll(patient.nombre)
      val gender = if (patient.sexoDto == "female" || patient.sexoDto.trim == "FEMENINO") "female" else "male"
      patient.copy(nombre = name, apellido = lastName, apellidoCasada = motherLastName, sexoDto = gender)
    }
  }
}

trait Sanitizer[T] {
  def sanitize: T
}