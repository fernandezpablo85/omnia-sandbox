package models

import java.util.Date
import play.api.libs.json._
import play.api.Logger

case class Evolution(id: Option[Long], patient: Long, description: String, date: Long, created: Long,
  overridenBy: Option[Long], episode: Long, fqe: Option[String]) {

  lazy val edited = overridenBy.isDefined && overridenBy.get > 0
  lazy val deleted = overridenBy.isDefined && overridenBy.get < 0

  def asJson(problems: List[Problem]) = {
    import Evolution._
    implicitly[Writes[Evolution]]
    val json = Json.toJson(this).as[JsObject]
    val problemJson = Json.toJson(problems).as[JsArray]
    json + (("problems", problemJson))
  }
}

object Evolution {
  // types needed to be referenced from 'asJson' in evolution. Weird right?
  // http://stackoverflow.com/questions/23657978/playframeworks-json-writes-implicit-requires-explicit-type-why
  implicit val problemWrites: Writes[Problem] = Problem.writes
  implicit val problemReads: Reads[Problem] = Problem.reads
  implicit val writes: Writes[Evolution] = Json.writes[Evolution]

  // needed since 'created' is not optional but assigned on creation.
  implicit val reads = new Reads[Evolution] {
    val default = Json.reads[Evolution]
    def reads(json: JsValue): JsResult[Evolution] = {
      val withTimestamp = json.as[JsObject] ++ Json.obj("created" -> System.currentTimeMillis)
      default.reads(withTimestamp)
    }
  }
}

case class Problem(id: Option[Long], episode: Long, name: String, started: Option[Long], patient: Long, state: String,
  created: Long, editedBy: Option[Long], code: Option[String], note: Option[String], severity: Option[String],
  ended: Option[Long], familyRelation: Option[String]) {

  // validate start-end period.
  if (started.isDefined && ended.isDefined){
    require(started.get < ended.get, "invalid started-ended range")
  }

  lazy val isFamilyProblem = state == "family"

  lazy val asAllergy = Allergy(
    id = id,
    episode = episode,
    substance = name,
    patient = patient,
    started = started,
    category = state,
    reaction = note.getOrElse(""),
    severity = severity.getOrElse(""),
    created = created,
    editedBy = editedBy
  )
}

object Problem {
  implicit val writes = Json.writes[Problem]
  implicit val reads = Json.reads[Problem]
}

case class Allergy(id: Option[Long], episode: Long, substance: String, patient: Long, started: Option[Long],
  category: String, reaction: String, severity: String, created: Long, editedBy: Option[Long]) {

  lazy val asProblem = Problem(
    id = id,
    episode = episode,
    name = substance,
    started = started,
    patient = patient,
    state = category,
    created = created,
    editedBy = editedBy,
    note = Some(reaction),
    severity = Some(severity),
    code = None,
    ended = None,
    familyRelation = None
  )
}

object Allergy {
  implicit val writes = Json.writes[Allergy]
  implicit val reads = Json.reads[Allergy]
}

case class Treatment(id: Option[Long], name: String, kind: String, dose: Option[String], via: Option[String],
  created: Option[Long], ownerId: Long, fav: Boolean = false, description: Option[String], frequencyInHours: Option[Int])

object Treatment {
  implicit val writes = Json.writes[Treatment]
  implicit val reads = Json.reads[Treatment]
}

case class Indication(id: Option[Long], treatmentId: Long, problemId: Option[Long], starts: Option[Long], chronic: Boolean, durationInDays: Option[Int],
  comments: Option[String], state: String, created: Option[Long], patientId: Long)

object Indication {
  implicit val writes = Json.writes[Indication]
  implicit val reads = Json.reads[Indication]
}

case class Attachment(id: Option[Long], patient:Long, name: String, url: String, kind: String, parentId: Long, created: Long)

object Attachment {
  val ContingencyKind = "contingency"
  implicit val writes = Json.writes[Attachment]
  implicit val reads = Json.reads[Attachment]

  def contingency(id: Long, file: ContingencyFile, patient: Long) =
    Attachment(None, patient, file.name, file.url, Attachment.ContingencyKind, id, System.currentTimeMillis)
}

case class Result(id: Option[Long], patient: Long, category: String, resultType: String, description: Option[String], issued: Option[Long], created: Long, editedBy: Option[Long], ownerId: Long, solicitationId: Option[Long], fqe: Option[String])

object Result {
  implicit val writes = Json.writes[Result]
  implicit val reads = Json.reads[Result]
}

case class TimelineEntry(id: Option[Long], kind: String, data1: Option[Long], data2: Option[Long], data3: Option[Long],
  ownerId: Option[Long], created: Long, day: Long, patientId: Option[Long], episodeId: Option[Long],
  specialty: Option[String], fqe: Option[String], location: Option[String])

object TimelineEntry {

  implicit val writes = Json.writes[TimelineEntry]
  implicit val reads = Json.reads[TimelineEntry]

  private def fqe(episode: Long, udn: String) = Some(Episode(episode, udn).fqe)
  private def now = System.currentTimeMillis
  private def day(ts: Long = now) = Math.ceil(ts / 1000.0 / 3600.0 / 24.0).toLong

  def newProblem(problemId: Long, ownerId: Long, patientId: Long, episodeId: Long, specialty: String, udn: String) =
    TimelineEntry(None, "problem.created", Some(problemId), None, None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), None)

  def newBackground(problemId: Long, ownerId: Long, patientId: Long, episodeId: Long, specialty: String, udn: String) =
    TimelineEntry(None, "background.created", Some(problemId), None, None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), None)

  def editProblem(oldId: Long, newId: Long,  ownerId: Long, patientId: Long, episodeId: Long, specialty: String, udn: String) =
    TimelineEntry(None, "problem.edited", Some(oldId), Some(newId), None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), None)

  def newEvolution(evolutionId: Long, ownerId: Long, patientId: Long, when: Long, specialty: String, episodeId: Long, udn: String, location: Option[String]) =
    TimelineEntry(None, "evolution.created", Some(evolutionId), None, None, Some(ownerId), when, day(when), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), location)

  def deleteEvolution(evolutionId: Long, ownerId: Long, patientId: Long, specialty: String, episodeId: Long, udn: String) =
    TimelineEntry(None, "evolution.deleted", Some(evolutionId), None, None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), None)

  def editEvolution(evolutionId: Long, ownerId: Long, patientId: Long, overridenBy: Long, specialty: String, episodeId: Long, udn: String, location: Option[String]) =
    TimelineEntry(None, "evolution.edited", Some(evolutionId), Some(overridenBy), None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), location)

  def newVitalSign(medicalInfoId: Long, ownerId: Long, patientId: Long, specialty: String, episodeId: Long, udn: String) =
    TimelineEntry(None, "vitalsign.created", Some(medicalInfoId), None, None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), None)

  def deletedVitalSign(medicalInfoId: Long, ownerId: Long, patientId: Long, specialty: String, episodeId: Long, udn: String) =
    TimelineEntry(None, "vitalsign.deleted", Some(medicalInfoId), None, None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), None)

  def closeMedicalInfo(medicalInfoId: Long, ownerId: Long, patientId: Long, specialty: String, episodeId: Long, udn: String, location: Option[String]) =
    TimelineEntry(None, "medicalInfo.closed", Some(medicalInfoId), None, None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), location)

  def editMedicalInfo(evolutionId: Long, ownerId: Long, patientId: Long, overridenBy: Long, specialty: String, episodeId: Long, udn: String, location: Option[String]) =
    TimelineEntry(None, "medicalInfo.edited", Some(evolutionId), Some(overridenBy), None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), location)

  def deleteMedicalInfo(medicalInfoId: Long, ownerId: Long, patientId: Long, specialty: String, episodeId: Long, udn: String) =
    TimelineEntry(None, "medicalInfo.deleted", Some(medicalInfoId), None, None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), None)

  def newSolicitation(solId: Long, ownerId: Long, patientId: Long, specialty: String, episodeId: Long, udn: String, location: Option[String]) =
    TimelineEntry(None, "solicitation.created", Some(solId), None, None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), location)

  def deleteSolicitation(solId: Long, ownerId: Long, patientId: Long, specialty: String, episodeId: Long, udn: String) =
    TimelineEntry(None, "solicitation.deleted", Some(solId), None, None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), None)

  def conciliateProblem(patientId: Long, problemId: Long, episodeId: Long, ownerId: Long, specialty: String, udn: String) =
    TimelineEntry(None, "problem.conciliated", Some(problemId), None, None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), None)

  def contingency(contingencyId: Long, patientId: Long, episodeId: Long, ownerId: Long, specialty: String, udn: String) =
    TimelineEntry(None, "contingency.created", Some(contingencyId), None, None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), None)

  def contingencyDeleted(contingencyId: Long, patientId: Long, episodeId: Long, ownerId: Long, specialty: String, udn: String) =
    TimelineEntry(None, "contingency.deleted", Some(contingencyId), None, None, Some(ownerId), now, day(), Some(patientId), Some(episodeId), Some(specialty), fqe(episodeId, udn), None)
}

case class MedicalInfo(id: Option[Long], patientId: Long, kind: String, value: String, status: String,
  created: Long, updated: Option[Long], updatedBy: Option[Long], episode: Long, fqe: Option[String])

object MedicalInfo {
  implicit val writes = Json.writes[MedicalInfo]
  implicit val reads = Json.reads[MedicalInfo]
}

case class Preferences(id: Option[Long], sessionId: Long, value: String, kind: String)

case class VitalSigns(height: Option[MedicalInfo], weight: Option[MedicalInfo], pressure: Option[MedicalInfo], frequency: Option[MedicalInfo])
object VitalSigns {
  implicit val writes = Json.writes[VitalSigns]
}

case class Appointment(id: Option[Long], owner: Long, patient: Long, date: Long, state: String, created: Long, author: Long)
object Appointment {
  implicit val writes = Json.writes[Appointment]
  implicit val reads = Json.reads[Appointment]
}

case class LogEntry(date: String, title: String, extra: List[String], author: String, specialty: Option[String], location: Option[String])
object LogEntry {
  implicit val reads = Json.reads[LogEntry]
}

// 'estado' shouldn't be optional but udn=3 still does not send the value.
case class Admission(admision: Long, ingreso: String, paciente: Patient, habitacion: Option[String],
  cama: Option[String], sector: Option[String], piso: Option[String], fechaAlta: Option[String], cobertura: Option[HealthCare], condicionIva: Option[String], internacionId: Long, estado: Option[String])
case class Patient(id: Long, apellido: String, apellidoCasada: Option[String], nombre: String, fechaNacimiento: String, documento: String,
  sexoDto: String, tipoDocumento: Option[String])
case class HealthCare(financiador: String, plan: Option[String], numero: Option[String])

object Admission {
  implicit val hcreads = Json.reads[HealthCare]
  implicit val hcwrites = Json.writes[HealthCare]
  implicit val preads = Json.reads[Patient]
  implicit val pwrites = Json.writes[Patient]
  implicit val reads = Json.reads[Admission]
  implicit val writes = Json.writes[Admission]
}

case class Center(id: Long, nombre: String, codigo: String, udnDto: Clinic)
case class Clinic(id: Long, codigo: String,  nombre: String)
case class Role(id: Long, codigo: String, descripcion: String)
case class Sector(id: Long, descripcion: String)
case class Specialty(id: Long, codigo: String, nombre: String, descripcion: String)
case class SubSpecialty(id: Long, descripcion: String, descripcionLarga: String, especialidad: Option[Specialty])

case class License(id: Long, tipoMatricula: String, numeroMatricula: String) {
  override def toString = tipoMatricula match {
    case "NACIONAL" => s"MN: $numeroMatricula"
    case _ => s"MP: $numeroMatricula"
  }
}

// sometimes specialties are empty.
object NullSpecialty extends SubSpecialty(-1, "administrativo", "long-descrip", None)

case class ActiveUserInfo(data: CookieUserInfo, activeCenter: Center, activeSpecialty: SubSpecialty)

object UserInfoWithRequestId {
  implicit val readsRole = Json.reads[Role]
  implicit val readsSpecialty = Json.reads[Specialty]
  implicit val readsSubSpecialty = Json.reads[SubSpecialty]
  implicit val readsSector = Json.reads[Sector]
  implicit val readsClinic = Json.reads[Clinic]
  implicit val readsCenter = Json.reads[Center]
  implicit val readsLicense = Json.reads[License]
  implicit val reads = Json.reads[UserInfoWithRequestId]
}

object CookieUserInfo {
  implicit val writesRole = Json.writes[Role]
  implicit val writesSpecialty = Json.writes[Specialty]
  implicit val writesSubSpecialty = Json.writes[SubSpecialty]
  implicit val writesSector = Json.writes[Sector]
  implicit val writesClinic = Json.writes[Clinic]
  implicit val writesCenter = Json.writes[Center]
  implicit val writesLicense = Json.writes[License]
  implicit val writes = Json.writes[CookieUserInfo]

  implicit val readsRole = Json.reads[Role]
  implicit val readsSpecialty = Json.reads[Specialty]
  implicit val readsSubSpecialty = Json.reads[SubSpecialty]
  implicit val readsSector = Json.reads[Sector]
  implicit val readsClinic = Json.reads[Clinic]
  implicit val readsCenter = Json.reads[Center]
  implicit val readsLicense = Json.reads[License]
  implicit val reads = Json.reads[CookieUserInfo]
}

// in spanish because that's how we get it from the api.
case class UserInfoWithRequestId(id: Long, nombre: String, apellido: String, centros: List[Center],
  subEspecialidades: List[SubSpecialty], roles: List[Role], matricula: Option[License], requesterId: String)

case class CookieUserInfo(id: Long, nombre: String, apellido: String, centros: List[Center],
  subEspecialidades: List[SubSpecialty], roles: List[Role], matricula: Option[License]) {

  lazy val json = Json.stringify(Json.toJson(this))
  def toTuples: Seq[(String, String)] = Seq("id" -> id.toString, "__data__" -> json)

  def getSpecialty(id: Long) = {
    if (id < 0 && subEspecialidades.length == 0) {
      Some(NullSpecialty)
    } else {
      val spec = subEspecialidades.find(_.id == id)
      if (spec.isEmpty) {
        Logger.warn(s"specialty with id ${id} not found, specialties: ${subEspecialidades}")
      }
      spec
    }
  }

  def getCenter(id: Long) = {
    val center = centros.find(_.id == id)
    if (center.isEmpty) {
      Logger.warn(s"center with id ${id} not found, centers: ${centros}")
    }
    center
  }

  @deprecated("not present anymore", "1.0")
  def requesterId = ""
}

case class Solicitation(id: Option[Long], kind: String, status: String, description: String,
  conditions: String, ownerId: Long, patientId: Long, created: Long, fqe: Option[String])

object Solicitation {
  implicit val reads = Json.reads[Solicitation]
  implicit val writes = Json.writes[Solicitation]
}

case class Practice(id: Option[Long], pid: String, kind: String, label: String, abbr: Option[String], extra: Option[String], display: Boolean = true)

object Practice {
  implicit val reads = Json.reads[Practice]
  implicit val writes = Json.writes[Practice]
}

case class ShortPractice(id: Long, label: String)

object ShortPractice {
  implicit val reads = Json.reads[ShortPractice]
  implicit val writes = Json.writes[ShortPractice]
}

case class Author(id: Long, userName: String, firstName: String, lastName: String, license: Option[String])

object Author {
  implicit val reads = Json.reads[Author]
  implicit val writes = Json.writes[Author]
}

case class Episode(number: Long, udn: String) {
  val outpatient = (number == 0)
  val fqe = udn + "-" + number
}

case class Contingency(id: Option[Long], fqe: String, author: Long, kind: String, generated: Long, created: Long, status: String)

object Contingency {
  implicit val reads = Json.reads[Contingency]
  implicit val writes = Json.writes[Contingency]
}

case class ContingencyFile(name: String, url: String)
object ContingencyFile {
  implicit val reads = Json.reads[ContingencyFile]
}
