package util

import org.slf4j.MDC
import scala.concurrent.{ExecutionContextExecutor, ExecutionContext}
import java.util.{Map => JavaMap}

class ContextAwareExecutor(context: JavaMap[String, String], delegate: ExecutionContext) extends ExecutionContextExecutor {

  def reportFailure(t: Throwable): Unit = delegate.reportFailure(t)

  def execute(runnable: Runnable): Unit = {
    val wrapped = new ContextAwareRunnable(context, runnable)
    delegate.execute(wrapped)
  }

  private class ContextAwareRunnable(context: JavaMap[String, String], delegate: Runnable) extends Runnable {

    def run() = {
      val oldContext = MDC.getCopyOfContextMap

      // set inherited context.
      if (context == null) MDC.clear() else MDC.setContextMap(context)

      try {
        delegate.run()
      } finally {
        if (oldContext != null) MDC.setContextMap(oldContext)
      }
    }
  }
}

object ContextAwareExecutor {
  import play.api.libs.concurrent.Execution

  object Implicits {

    implicit def defaultContext: ExecutionContext = {
      new ContextAwareExecutor(MDC.getCopyOfContextMap, Execution.defaultContext)
    }
  }
}