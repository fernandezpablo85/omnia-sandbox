package util

import play.api.Play.current
import play.api.{Play, Mode}
import play.api.Logger
import scala.collection.concurrent.TrieMap

object Configuration {

  // misc.
  val AppName = current.configuration.getString("app.name").getOrElse("dev")
  val AppVersion = Option(getClass.getPackage.getImplementationVersion).getOrElse("dev")
  val MixpanelToken = current.configuration.getString("mixpanel.token").get
  val Stealth = current.configuration.getBoolean("stealth").get
  val HttpsOn = current.configuration.getBoolean("https").get
  val SessionTimeout = current.configuration.getMilliseconds("session.timeout").getOrElse(0L)
  val ApplyEvolutions = current.configuration.getBoolean("applyEvolutions.default").get

  // metrics
  val MetricsEnabled = current.configuration.getBoolean("metrics.enabled").get
  val MetricsApiKey = current.configuration.getString("metrics.key").get
}