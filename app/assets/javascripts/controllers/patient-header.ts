/// <reference path="../lib/moment.d.ts" />
/// <reference path="../lib/underscore.d.ts" />
/// <reference path="../lib/angular.d.ts" />
/// <reference path="../model.d.ts" />
/// <reference path="../services.d.ts" />
declare var ehrControllers;
ehrControllers.controller('PatientHeader', ['$scope', '$window', '$routeParams', 'MedicalInfo', '$rootScope', '$location', 'Patients',
  function ($scope, $window, $routeParams, MedicalInfo, $rootScope, $location, Patients: services.PatientService) {
    'use strict';

    $scope.showExtendedProfile = false;
    $scope.noTemplates = true;

    MedicalInfo.getAvailableTemplates().then(function (templates) {
      $scope.templates = templates;
      $scope.noTemplates = (templates && Object.keys(templates).length <= 0);
    });

    $scope.openPdfInNewTab = function() {
      Omnia.trackEvent('download pdf (desde el header)', {profile: $routeParams.id});
      $window.open('/patients/' + $routeParams.id + '/profile?download');
    };

    $scope.openCalculators = function() {
      Omnia.trackEvent('click en Calculadores', {profile: $routeParams.id});
      $location.path('/patients/' + $routeParams.id + '/calculators');
    };

    $scope.timelineUrl = function(episode: number) {
      return `/patients/${$routeParams.id}/episodes/${episode}/evolutions`;
    };

    $scope.urlRegexp = function(episode: number) {
      return `patients/${$routeParams.id}/episodes/${episode}/.*`;
    };

}]);
