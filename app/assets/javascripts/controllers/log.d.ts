/// <reference path="../model.d.ts" />
declare module log {
  interface Filter {
    author: model.Author;
    action: string;
    start: number;
    end: number;
  }
}