/// <reference path="../../lib/angular.d.ts" />
/// <reference path="../../lib/underscore.d.ts" />
/// <reference path="../../lib/moment.d.ts" />
/// <reference path="../../lib/bootstrap.d.ts" />
/// <reference path="../../model.d.ts" />
/// <reference path="../../services.d.ts" />
declare var ehrControllers;
ehrControllers.controller('Ingreso', ['$scope', '$routeParams', 'Problems', '$rootScope',
  function ($scope, $routeParams, Problems: services.ProblemService, $rootScope) {

    $scope.steps = [
      {'nbr': 1, 'title': 'Datos de ingreso'},
      {'nbr': 2, 'title': 'Reconciliación de problemas activos'},
      {'nbr': 3, 'title': 'Antecedentes patológicos'},
      {'nbr': 4, 'title': 'Medicación en Ambulatorio'},
      {'nbr': 5, 'title': 'Enfermedad actual'},
      {'nbr': 6, 'title': 'Estudios complementarios realizados'},
      {'nbr': 7, 'title': 'Interpretación diagnóstica'},
      {'nbr': 8, 'title': 'Plan de estudio y tratamiento'},
    ];

    $scope.data.problems = [];

    var defaultData = { 'fecha': moment().format('DD/MM/YYYY'), 'hora': moment().format('HH:mm') };
    $scope.data._1 = $scope.data._1 || defaultData;

    $scope.problemsToSolve = {};

    var step2Completed = () => {
      _($scope.activeProblems).each(p => {
        if ($scope.problemsToSolve[p.id]) {
          $scope.solveProblem(p).then(solved => {
            $scope.data._3.createdProblems = _($scope.data._3.createdProblems).map(pr => {
              if (pr.id === solved.id) {
                pr = angular.copy(Problems.addDisplayAttrs(solved));
              }
              return pr;
            });
          });
        } else {
          $scope.conciliateProblem(p);
        }
      });
    };

    const showNewProblemModal = () => {
      $rootScope.$broadcast('problem-reset');
      $('.new-problem-modal-ingreso').modal('show');
    }
    $scope.showNewProblemModal = showNewProblemModal

    var unbindCreated = $rootScope.$on('problem.created', function (event, problem) {
      $('.modal').modal('hide');
      if ($scope.step === 3) {
        $scope.data._3 = $scope.data._3 || {};
        $scope.data._3.createdProblems = $scope.data._3.createdProblems || [];
        problem._selected = true;
        $scope.data._3.createdProblems.push(problem);
        $scope.changeMarkedProblems($scope.data._3);
      } else if ($scope.step === 7) {
        $scope.selectableProblems.push(problem);
        $scope.selectedProblems.push(problem._token);
      }
    });
    $scope.$on('$destroy', unbindCreated);

    $scope.$on('medical-info-after-step', (ev, data) => {
      if (data.step === 2 && $scope.info.status === 'open') {
        step2Completed();
      }
    });

    $scope.$on('medical-info-before-step', (ev, context) => {
      if (context.step === 3 && $scope.info.status === 'open') {
        initStep3();
      } else if (context.step === 7) {
        initStep7();
      }
    });

    $scope.$watchCollection('selectedProblems', (problemTokens) => {
      var findProblemsByToken = function (tokens) {
        var problems = _($scope.selectableProblems).filter(function (p) { return tokens.indexOf(p._token) >= 0; });
        return angular.copy(problems);
      };
      if (problemTokens.length > 0) {
        var problems = findProblemsByToken(problemTokens);
        $scope.data._7 = $scope.data._7 || {};
        $scope.data._7.createdProblems = problems;
        $scope.data.problems = problems;
      }
    });

    var initStep3 = () => {
      Problems.getProblemsAndAllergiesForPatient($routeParams.id).then(function (problems) {
        if (!$scope.data._3) {
          $scope.data._3 = {}
          $scope.data._3.createdProblems = _(problems).map(Problems.addDisplayAttrs);
          $scope.setAllChecks($scope.data._3, true);
        }
      });
    };

    var initStep7 = () => {
      var addToken = (problem: model.ProblemWithDisplayInfo) => {
        problem._token = problem.name + '|' + problem.state;
        return problem;
      };
      Problems.getProblemsAndAllergiesForPatient($routeParams.id).then(function (problems) {
        $scope.selectableProblems = _(Problems.asSelectableList(problems)).map(addToken);
        if ($scope.data._7 && $scope.data._7.createdProblems) {
          _($scope.data._7.createdProblems).each(p => $scope.selectedProblems.push(p._token));
        }
      });
    };

}]);
