/// <reference path="../../lib/jquery.d.ts" />
/// <reference path="../../lib/underscore.d.ts" />
/// <reference path="../../lib/angular.d.ts" />
/// <reference path="../../model.d.ts" />
/// <reference path="../../services.d.ts" />
ehrControllers.controller('Anestesia', ['$scope', 'Problems', '$routeParams',
    function ($scope, Problems, $routeParams) {
        /*
          Los pasos de la ficha. Son requeridos.

          nbr = Número (number) del paso de la ficha.
          title = Título del paso de la ficha
        */
        $scope.steps = [
            { 'nbr': 1, 'title': 'Ejemplo Paso 1' },
            { 'nbr': 2, 'title': 'Ejemplo Paso 2' }
        ];
        function crearProblemaDeEjemplo() {
            var existingProblems = [];
            return Problems.getOrCreate('Problema de ejemplo', 'active', existingProblems, $routeParams.id, $routeParams.episodeId);
        }
        /*
          Toda ficha está asociada a uno o más problemas, en este caso se genera
          uno de ejemplo.
        */
        var problem = crearProblemaDeEjemplo();
        $scope.data.problems = [problem];

        var roundMinutes = Math.round(moment().minute() / 5) * 5;
        var defaultData = { 'fecha': moment().format('DD/MM/YYYY'), 'hora': moment().minute(roundMinutes).format('HH:mm') };
        $scope.data._1 = $scope.data._1 || defaultData;

        var parte = {'id': '543643'};
        $scope.data.parte = $scope.data.parte || parte;

        var procedimientos = [{'id': 1, 'name': 'Convencional'},
                                {'id': 2, 'name': 'Ambulatoria'},
                                {'id': 3, 'name': 'Alta complejidad'}];
        $scope.procedimientos = $scope.procedimientos || procedimientos;

        var anestesias = [{'id': 1, 'name': 'Inhalatoria'},
                                {'id': 2, 'name': 'Endovenosa'},
                                {'id': 3, 'name': 'Regional'},
                                {'id': 4, 'name': 'Local'}];
        $scope.anestesias = $scope.anestesias || anestesias;

        var selectedProcedimientos = [];
        $scope.selectProcedimiento = function ($proc, $element){
            var procedimientoExists = selectedProcedimientos.indexOf($proc.procedimiento.id);
            if (procedimientoExists == -1){
                selectedProcedimientos.push($proc.procedimiento.id);
                angular.element($element.currentTarget).addClass('multiBtnSelected');
            }
            else {
                selectedProcedimientos.splice(procedimientoExists, 1);
                angular.element($element.currentTarget).removeClass('multiBtnSelected');
            }
            $scope.data._1.selectedProcedimientos = selectedProcedimientos;
            $scope.persistData();
        };

        var selectedAnestesias = [];
        $scope.selectAnestesia = function ($proc, $element){
            var anestesiaExists = selectedAnestesias.indexOf($proc.anestesia.id);
            if (anestesiaExists == -1){
                selectedAnestesias.push($proc.anestesia.id);
                angular.element($element.currentTarget).addClass('multiBtnSelected');
            }
            else {
                selectedAnestesias.splice(anestesiaExists, 1);
                angular.element($element.currentTarget).removeClass('multiBtnSelected');
            }
            $scope.data._1.selectedAnestesias = selectedAnestesias;
            $scope.persistData();
        };
    }]);