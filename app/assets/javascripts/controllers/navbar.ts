/// <reference path="../services.d.ts" />
/// <reference path="../lib/underscore.d.ts" />
/// <reference path="../lib/jquery.d.ts" />
/// <reference path="../lib/bootstrap.d.ts" />
declare var ehrControllers: any;

interface NavItem {
  id: number;
  url: string
  label: string;
  regex: RegExp;
  newTab?: boolean;
  onlyWhenSelected?: boolean;
  subitems: NavSubitem[];
  toolsMenu?: NavSubitem[];
}

interface NavSubitem {
  url: () => string;
  label: string;
  regex?: string;
  nopatient?: boolean;
}

ehrControllers.controller('NavBar', ['$scope', '$rootScope', '$http', 'Alerts', '$route', '$location','$timeout', 'UserInfo', 'Confirm', '$routeParams',
  function($scope, $rootScope, $http, Alerts, $route, $location, $timeout, UserInfo: services.UserInfoService, Confirm: services.ConfirmService, $routeParams) {

  // DOM stuff.
  $('nav.navbar span.with-tooltip').tooltip();

  var patientMenu: NavItem = {
    'id': 0, 'url': '/patients', 'label': 'Pacientes', 'regex': /\/patients$|search-patients/,
    'subitems': [{ 'url': () => '/patients', 'label': 'Pacientes internados', 'nopatient': true },
      { 'url': () => '/search-patients', 'label': 'Buscar Pacientes', 'nopatient': true }]
  };

  var toolsMenu = [{ 'url': () => $scope.patientEpisodeUrl + '/log', 'label': 'Registro de acciones' }];
  var readOnlySubitems = [{ 'url': () => $scope.patientEpisodeUrl + '/log', 'label': 'Registro de acciones', 'regex': 'patients.*[^contingency]$' }];

  if (UserInfo.hasContingencyConciliation()) {
    toolsMenu.push({ 'url': () => $scope.patientEpisodeUrl + '/contingency', 'label': 'Reconciliar Contingencia' });
    readOnlySubitems.push({ 'url': () => $scope.patientEpisodeUrl + '/contingency', 'label': 'Reconciliar Contingencia', 'regex': 'patients.*contingency$' });
  }

  var items: NavItem[] = [ patientMenu,
    {
      'id': 1, 'url': '', 'label': 'Historia Clínica', 'regex': /\/patients\/\d+\/.+/, 'onlyWhenSelected': true,
      'subitems': []
    }
  ];

  var readOnlyItems: NavItem[] = [ patientMenu,
    {
      'id': 1, 'url': '', 'label': 'Historia Clínica', 'regex': /\/patients\/\d+\/.+/, 'onlyWhenSelected': true,
      'subitems': readOnlySubitems
    }
  ];

  var getPatientEpisodeUrl = (patient: model.Patient) => {
    var episodeId = $routeParams.episodeId;
    return `/patients/${patient.id}/episodes/${episodeId}`;
  }

  $scope.navigation = UserInfo.hasWriteAccess() ? items : readOnlyItems;

  $rootScope.$on('active.patient', function (_, data) {
    $scope.activePatient = data.patient;
    $scope.patientEpisodeUrl = getPatientEpisodeUrl(data.patient)
  });

  $scope.pick = (id: number) => {
    var picked = _($scope.navigation).find((it) => it.id === id);
    if (picked.newTab) {
      window.open(picked.url, 'help_page', 'menubar=0, scrollbar=0, location=0, status=0, toolbar=0, titlebar=0');
    } else {
      $scope.picked = picked;
    }
  };

  $scope.$watch('picked', function (current: NavItem, old: NavItem) {
    if (current && current !== old) {
      $location.url(current.url);
    }
  });

  var findItemByUrl = (url: string) => {
    return _($scope.navigation).find((it) => !!url.match(it.regex));
  };

  $scope.promptExit = function() {
    Confirm.show({ 'message': '¿Desea salir de Omnia Salud?'}).then(() => {
      window.location.href = '/logout';
    });
  };

  $scope.location = $location;
  $scope.$watch('location.url()', function (path, old) {
    if (!path) return;
    $scope.picked = findItemByUrl(path);
  });
}]);