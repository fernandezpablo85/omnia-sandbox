/// <reference path="model.d.ts" />
/// <reference path="api.d.ts" />
/// <reference path="lib/angular.d.ts" />
declare module services {
  interface UserInfoService {
    getUserInfo: () => any;
    hasWriteAccess: () => boolean;
    hasPrintAccess: () => boolean;
    hasDisabledAutoLogoff: () => boolean;
    hasContingencyConciliation: () => boolean;
    isOutPatient: () => boolean;
    useInternalDbId: () => boolean;
  }
  interface ConfirmOpts {
    title?: string;
    message: string;
  }
  interface ConfirmService {
    show: (opts: ConfirmOpts) => ng.IPromise<boolean>;
  }
  interface SolicitationService {
    save: (patient: number, episode:number, opts: model.SolicitationJson) => angular.IHttpPromise<any>;
    updateStatus: (patientId: number, episode: number, sId: number, newStatus: string) => angular.IHttpPromise<any>;
    kinds: SolicitationKinds[];
    forPatientAndEpisode: (patientId: number, episode: number) => angular.IHttpPromise<{ solicitations: model.Solicitation[], authors: model.Author[] }>
    conditionsMoment: ConditionMoments;
    conditionsSample: any;
    conditionsTransport: any;
    conditionsYesNo: any;
  }
  interface ConditionMoments {
    [key: string]: ConditionMoment;
  }
  interface ConditionMoment {
    style: string;
    label: string;
    text: string;
  }
  interface SolicitationKinds {
    name: string;
    label: string;
  }

  interface AlertsService {
    info: (message: string, options?: any) => void;
    error: (message: string, options?: any) => void;
    warn: (message: string, options?: any) => void;
    success: (message: string, options?: any) => void;
    clearAll: () => void;
    clearLast: () => void;
  }

  interface ResultService {
    forPatient: (patientId: number, episodeId: number) => angular.IPromise<{ results: model.Result[], authors: any[] }>;
    deleteResult: (resultId: number, patientId: number, episodeId: number) => angular.IHttpPromise<any>;
    save: (result: model.Result, patientId: number, episodeId: number) => angular.IPromise<any>;
    categories: model.ResultCategory[];
    getCategory: (name: string) => model.ResultCategory;
  }

  interface PracticeService {
    search: (kind: string, term: string) => angular.IHttpPromise<model.Practice[]>;
    isGroup: (pid: string) => boolean;
    getPracticesForGroup: (pid: string) => string[];
    getPracticesByIds: (kind: string, pids: string[]) => angular.IHttpPromise<model.Practice[]>;
    getSelectedPractices: (jsonString: string) => any;
  }

  interface EcgPractices {
    rhythms: string[];
    min: (key: string) => number;
    max: (key: string) => number;
    inRange: (key: string, value: number) => boolean;
    inSoftRange: (key: string, value: number) => boolean;
  }

  interface ProblemService {
    statuses: model.ProblemMetadata[];
    familyRelations: model.ProblemMetadata[];
    findFamilyRelation: (value: string) => model.ProblemMetadata;
    getProblemsForPatient: (patientId: number) => angular.IPromise<api.Problem[]>;
    getProblemsAndAllergiesForPatient: (patientId: number) => angular.IPromise<api.Problem[]>;
    getAllergiesForPatient: (patientId: number) => angular.IHttpPromise<api.Problem[]>;

    // TODO: define term search result type.
    search: (term: string) => angular.IHttpPromise<any[]>;
    updateBackground: (patientId: number, episodeId: number, problems: model.UnsavedProblem[]) => angular.IPromise<model.Problem[]>;
    status: (problem: model.Problem) => string;
    evolutionName: (problem: model.Problem) => string;
    addDisplayAttrs: (problem: api.Problem) => model.ProblemWithDisplayInfo;
    asSelectableList: (problems: api.Problem[]) => model.ProblemWithDisplayInfo[];
    severityLabels: { mild: string, moderate: string, severe: string };
    save: (problem: model.UnsavedProblem, params: {patientId: number, episodeId: number}) => angular.IPromise<{ id: number }>;
    conciliate: (patientId: number, problemId: number, episodeId: number) => angular.IHttpPromise<any>;
    getOrCreate: (name: string, state: string, existingProblems: model.Problem[], patientId: number, episodeId: number) => any;
  }

  interface PatientSearchParams {
    firstName?: string;
    lastName?: string;
  }

  interface PatientService {
    get: (patientId: number) => angular.IPromise<model.Patient>;
    all: () => angular.IHttpPromise<model.Admission[]>;
    search: (params: PatientSearchParams) => angular.IHttpPromise<model.Admission[]>;
    admissionsToPatient: (admissions: model.Admission[]) => model.Patient;
    getAgeString: (bday: string) => string;
    fromNow: (timestamp: number, unit: string) => number;
    getOutPatients: () => ng.IHttpPromise<Array<model.OutPatient>>;
  }

  interface FlatTimeline {
    getByEpisode: (patientId: number, episode: number) => angular.IPromise<any>;
  }

  interface EvolutionService {
    save: (evolution: { patient: number, description: string, date: number|string },
      evolutionProblemIds: { id: number }[], episodeId: number, location: string) => ng.IHttpPromise<{}>;
    remove: (evolution: { id: number }) => ng.IHttpPromise<{}>;
  }

  interface MedicalInfoService {
    save: (patientId: number, episode: number, info: model.MedicalInfo, location: string) => any;
    updateStatus: (patientId: number, episode: number, newStatus: string, info: model.MedicalInfo, location: string) => any;
    remove: (patientId: number, episode: number, info: model.MedicalInfo) => any;
    getAvailableTemplates: () => ng.IPromise<{}>;
    forEpisodeAndStatus: (patientId: number, episodeId: number, status: string) => angular.IHttpPromise<model.MedicalInfo[]>;
    forEpisodeAndKind: (patientId: number, episodeId: number, kind: string) => angular.IHttpPromise<model.MedicalInfo[]>;
    getTitle: (name: string) => string;
    getEvolutionTemplates: () => ng.IPromise<{}>;
    getEvolutionHelpers: () => {};
  }

  interface OmniaGlobal {
    currentUserId: number;
    currentUserName: string;
    currentUserLicense: string;
    currentSpecialty: string;
    setUserEmail: (email: string) => void;
    initEventTrackers: () => void;
    trackEvent: (eventName: string, eventData?: any) => void;
    incrementCounter: (counterName: string) => void;
  }

  interface ContingencyService {
    save: (patientId: number, episode: number, contingency: model.Contingency) => angular.IHttpPromise<model.Contingency[]>;
    getByFqe: (patientId: number, episode: number) => angular.IHttpPromise<model.Contingency[]>;
    remove: (patientId: number, episode: number, contingencyId: number) => angular.IHttpPromise<void>;
    humanKind: (kind: string) => string;
    getKinds: () => { value: string, label: string }[];
  }

  interface S3Service {
    upload: (file: File) => ng.IHttpPromise<any[]>;
    uploadMulti: (files: File[]) => ng.IPromise<string[]>;
  }

  interface LogService {
    info: (message: string) => void
    warn: (message: string) => void
    error: (message: string) => void
  }
}

declare var Omnia: services.OmniaGlobal, omniaServices: ng.IModule;
