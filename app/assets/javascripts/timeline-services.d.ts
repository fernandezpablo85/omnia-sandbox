/// <reference path="model.d.ts" />
interface TimelineUtils {
  createdOrder: (timeline: model.TimelineEntry[]) => model.TimelineEntry[];
  log: (any) => any;
  humanDate: (number) => string;
  humanHour: (number) => string;
  appendHumanDate: (timeline: model.TimelineEntry[]) => model.TimelineEntry[];
  appendHumanFormats: (timeline: model.TimelineEntry[]) => model.TimelineEntry[];
  renderables: (renderableKinds: string[]) => any;
  filterPristine: (timeline: model.TimelineEntry[]) => model.TimelineEntry[];
  unixDay: (number) => number;
  filterByEvolutionProblem: (problemName: string) => any;
  filterByAuthor: (authorId: number) => any;
  filterByDateRange: (start: number, end: number) => any;
}