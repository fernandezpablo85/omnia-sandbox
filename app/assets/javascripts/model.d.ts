declare module model {
  interface Attachment {
    url: string;
    name: string;
    kind: string;
  }
  interface Problem {
    id?: number;
    name: string;
    state: string;
    started: number;
    ended: number;
    familyRelation: string;
    note: string;
    severity: string;
    owner?: Author;
    evolutionName?: string;
    editedBy?: number;
    created?: number;
    patient?: number;
  }
  interface ProblemMetadata {
    value: string;
    label: string;
  }
  interface UnsavedProblem {
    name: string;
    state: string;
    started: number;
    id?: number;
  }
  interface ProblemWithDisplayInfo extends Problem {
    _label: string;
    _ordinal: number;
    _humanState: string;
    _tooltip: string;
    _token: string;
    _selected?: boolean;
  }
  interface Treatment {
    kind: string;
    dose: number;
    frequencyInHours: number;
  }
  interface Result {
    id: number;
    resultType: string;
    issued: number;
    issuedDisplay: string;
    description: string;
    patient: number;
    created: number;
    category: any;
    ownerId: number;
    evolution: string;
    solicitationId: number;
    solicitation: Solicitation;
    attachments: Attachment[];
  }
  interface ResultCategory {
    value: string;
    label: string;
    order: number;
  }
  interface Indication {
    id: number;
    starts: number;
    human: string;
    treatment: Treatment;
    comments: string;
    chronic: boolean;
    durationInDays: number;
  }
  interface Evolution {
    id: number;
    description: string;
    date: number;
    owner?: Author;
    overrides?: boolean;
    problems: Problem[];
    attachments: Attachment[];
  }
  interface Author {
    id: number;
    firstName?: string;
    lastName?: string;
    license?: string;
    fullName?: string;
  }
  interface TimelineEntry {
    kind: string;
    dirty: boolean;
    day: number;
    created: number;
    createdOverride?: number;
    _title?: string;
    _extra?: string[];
    evolution?: Evolution;
    indication?: Indication;
    problem?: Problem;
    result?: Result;
    medicalInfo?: MedicalInfo;
    solicitation?: Solicitation;
    author: Author;
    specialty: string;
    vitalSign?: any;
    current?: any;
    old?: any;
    location?: string;

    humanDate?: string;
    humanHour?: string;
    humanVitalSign?: string;
    humanIndication?: string;
  }

  interface Admission {
    admision: number;
    cama: string;
    habitacion: string;
    piso: string;
    ingreso: string;
    fechaAlta?: string;
    sector: string;
    torre: string;
    internacionId: number;
    paciente: Patient;
    admissionDays?: number;
  }

  interface Patient {
    id: number;
    nombre: string;
    apellido: string;
    tipoDocumento: string;
    documento: string;
    fechaNacimiento: string;
    sexoDto: string;
    episodes: Episode[];
    age?: string;
    ageInMonths?: number;
    ageInYears?: number;
    currentLocation?: string;
    healthCare?: HealthCare;
    iva?: string;
  }

  interface HealthCare {
    financiador: string;
    numero: string;
    plan?: string;
  }

  interface Episode {
    id: number;
    start: number;
    admissionId?: number;
    end?: number;
    state: string;
  }

  interface Practice {
    id: number;
    pid: string;
    label: string;
    abbr?: string;
    extra?: string;
    display: boolean;
  }

  interface Solicitation {
    id: number;
    description: any;
    conditions: any;
    selectedPractices?: Practice[];
    type: string;
    kind: string;
    status: string;
  }

  interface SolicitationJson {
    description: string;
    conditions: string;
    kind: string;
    status: string;
  }

  interface Transfusion {
    performed: string;
    beginDate?: string;
    beginTime?: string;
    endDate?: string;
    endTime?: string;
    complications?: string;
    volume?: string;
    reaction?: string;
    practice?: Practice;
  }

  interface MedicalInfo {
    id?: number;
    value: string;
    kind: string;
    status: string;
    created?: number;
    updated?: number;
    overrides?: boolean;
  }

  interface MedicalTemplate {
    name: string;
    label: string;
    controller: string;
    openRecord?: MedicalInfo;
    evolutionTemplate?: string;
  }

  interface OutPatient {
    turnoId: string;
    pacienteId: string;
    pacienteTipoDoc: string;
    pacienteNroDoc: string;
    pacienteApellido: string;
    pacienteNombre: string;
    pacienteSexo: string;
    financiador: string;
    plan: string;
    nroAfiliado: string;
    tipoTurno: string;
    fechaTurno: string;
    horaTurno: string;
  }

  interface Contingency {
    id?: number;
    kind: string;
    generated?: number;
    created?: number;
  }
}
