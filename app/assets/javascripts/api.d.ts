/*
  these are the equivalents of Model.scala
  if you add/delete/rename a field there don't forget to do the same here.
*/
declare module api {
  interface Problem {
    id?: number;
    episode?: number;
    started?: number;
    name: string;
    patient?: number
    state: string;
    created?: number;
    editedBy?: number;
    code?: string;
    note?: string;
    severity?: string;
    ended?: number;
    familyRelation?: string;
  }
}